//Comentar
/* Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada de acordo com o horário do dia (bom dia, boa tarde ou boa noite).
*/

namespace exercicio_5{

    const data = new Date();
    const hora = data.getHours();

    let name: string;
    name = "Fernanda";

    if(hora >=3 && hora <12)
    {
        console.log("Bom dia " + name);
    } else if(hora >=12 && hora <18){
        console.log("Boa tarde " + name);
    }else
    {
        console.log("Boa noite " + name);
    }

}