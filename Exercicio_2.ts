//Comentar
/* Faça um programa que receba as três notas, calcule e mostre a média ponderada e o conceito que segue a tabela
*/

//Inicio
namespace exercicio_2{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;
    nota1 = 10;
    nota2 = 5;
    nota3 = 4;
    peso1 = 2;
    peso2 = 3;
    peso3 = 5;

    let media = ((nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3))

    if(media >=8){
        console.log("O conceito é A " + media);
    }else if(media >=7 && media <8){
        console.log("O conceito é B " + media);
    }else if(media >=6 && media <7){
        console.log("O conceito é C " + media);
    }else if (media >=5 && media <6){
        console.log("O conceito é D"  + media);
    }else if (media >=0 && media <5){
        console.log("O conceito é E " + media);
    }
}
