namespace exemplos_condicoes {
let idade: number = 18;

if (idade >= 18)
{
    console.log("Pode dirigir");
} //fim bloco
else
{
    console.log("Não pode dirigir");
}

idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir");
}